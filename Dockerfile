FROM python:3.9

WORKDIR /app
COPY data data
COPY requirements.txt .

RUN pip3 install -r requirements.txt

COPY .env .
COPY execution_quality.py .

CMD ["python3", "-m",  "execution_quality"]
