build: 
	docker build -t execution-quality .

run: 
	docker run -v $(PWD)/data:/app/data/ execution-quality
