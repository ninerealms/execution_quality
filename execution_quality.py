import datetime
import hashlib
import hmac
import json
import logging
import time
from dataclasses import dataclass
from typing import Optional
import os

import pandas as pd
import requests
import retry


########################################################################################
# Config / Env
########################################################################################

logging.basicConfig(format="%(asctime)s - %(message)s", level=logging.INFO)


########################################################################################
# Helpers / Models
########################################################################################


@dataclass
class Venue:
    name: str
    base_url: str
    rfc_url: Optional[str] = None


@dataclass
class RFC:
    """
    request for quote
    """

    from_asset: str
    to_asset: str
    from_amount: float


def prep_rfc(venue: Venue, rfc: RFC, asset_map: dict) -> dict:
    """
    Takes generic RFC and return specific params for venue.
    """

    if venue.name == "changelly":
        return {
            "from": asset_map[venue.name][rfc.from_asset],
            "to": asset_map[venue.name][rfc.to_asset],
            "amount": str(rfc.from_amount),
        }
    if venue.name == "letsexchange":
        return {
            "from": asset_map[venue.name][rfc.from_asset],
            "to": asset_map[venue.name][rfc.to_asset],
            "amount": rfc.from_amount,
            "network_from": asset_map[venue.name][rfc.from_asset],
            "network_to": asset_map[venue.name][rfc.to_asset],
        }

    if venue.name == "cic":
        return {
            "from": asset_map[venue.name][rfc.from_asset],
            "to": asset_map[venue.name][rfc.to_asset],
            "amount": rfc.from_amount,
        }

    if venue.name == "changenow":
        return {
            "amount": rfc.from_amount,
            "pair": f"{asset_map[venue.name][rfc.from_asset]}_{asset_map[venue.name][rfc.to_asset]}",
        }

    if venue.name == "thorchain":
        return {
            "from_asset": asset_map[venue.name][rfc.from_asset],
            "to_asset": asset_map[venue.name][rfc.to_asset],
            "amount": str(int(rfc.from_amount * 1e8)),
        }

    raise Exception(f"venue {venue.name} not supported")


@retry.retry(Exception, delay=1, backoff=2, tries=3)
def get_json(
    url: str, valid_response_codes: list[int] = [200], method: str = "GET", **kwargs
) -> tuple[dict, Optional[str]]:
    """
    Given a URL, fetch and decode the returned JSON.
    Returns human error string in error cases.
    """
    try:
        if method == "POST":
            r = requests.post(url, **kwargs)
        else:
            r = requests.get(url, **kwargs)

        if r.status_code not in valid_response_codes:
            return {}, f"{url} returned invalid response code: {r.status_code}"
        return r.json(), None
    except requests.exceptions.ConnectionError:
        return {}, f"{url} failed to connect to host"
    except requests.exceptions.JSONDecodeError:
        return {}, f"{url} returned invalid JSON"


########################################################################################
# ExecutionQuality
########################################################################################


class ExecutionQuality:
    def __init__(self, venue_list: list[Venue], rfc_list: list[RFC]):
        self.venue_list = venue_list
        self.rfc_list = rfc_list
        self.getters = {
            "thorchain": self.thorchain_quote,
            "changelly": self.changelly_quote,
            "changenow": self.changenow_quote,
            "letsexchange": self.letsexchange_quote,
            "cic": self.cic_quote,
        }
        self.changelly_api_key = os.getenv("CHANGELLY_API_KEY")
        self.changelly_api_secret = os.getenv("CHANGELLY_API_SECRET")
        self.changenow_api_key =  os.getenv("CHANGENOW_API_KEY")
        self.letsexchange_api_key =  os.getenv("LETSEXCHANGE_API_KEY")

        self.ASSET_MAP = {
            "thorchain": {"ETH.ETH": "ETH.ETH", "BTC.BTC": "BTC.BTC"},
            "changelly": {"ETH.ETH": "eth", "BTC.BTC": "btc"},
            "changenow": {"ETH.ETH": "eth", "BTC.BTC": "btc"},
            "letsexchange": {"ETH.ETH": "ETH", "BTC.BTC": "BTC"},
            "cic": {"ETH.ETH": "eth", "BTC.BTC": "btc"},
        }

    def _get_quote_base(self, venue: Venue, method: str = "GET", **kwargs) -> dict:
        # switch based on url to pass into request.
        if venue.rfc_url:
            _url = venue.rfc_url
        else:
            _url = venue.base_url

        data, err = get_json(url=_url, method=method, **kwargs)

        if err:
            err_msg = f"Venue: {venue.name}, {err}"
            logging.error(err_msg)
            return {}
        else:
            return data

    def _parse_thorchain_data(self, data: dict) -> dict:
        return {
            "ts": time.time(),
            "venue": "thorchain",
            "in_asset": data["from_asset"],
            "in_amount": int(data["amount"]) / 1e8,
            "out_asset": data["to_asset"],
            "out_amount": int(data["expected_amount_out"]) / 1e8,
        }

    def _get_raw_thorchain_quote(self, venue: Venue, rfc: RFC):
        """
        example response:
        thorchain

        {'dust_threshold': '10000',
        'expected_amount_out': '92669491',
        'expiry': 1677628016,
        'fees': {'affiliate': '0', 'asset': 'ETH.ETH', 'outbound': '720000'},
        'inbound_address': 'bc1qhg8hdz7ldhts4wdfm0s0xd9hr8r6g4hfxh6zc0',
        'inbound_confirmation_blocks': 1, 'inbound_confirmation_seconds': 600, 'notes': 'First output should be to inbound_address, second output should be change back to self, third output should be OP_RETURN, limited to 80 bytes. Do not send below the dust threshold. Do not use exotic spend scripts, locks or address formats (P2WSH with Bech32 address format preferred).', 'outbound_delay_blocks': 9, 'outbound_delay_seconds': 108, 'slippage_bps': 2, 'warning': 'Do not cache this response. Do not send funds after the expiry.'}
        """
        query_params = prep_rfc(venue, rfc, self.ASSET_MAP)

        headers = {"Content-type": "application/json", "Accept": "application/json"}
        data = self._get_quote_base(venue=venue, params=query_params, headers=headers)
        # add query params to data to reference in parsing.
        data.update(query_params)
        return data

    def thorchain_quote(self, venue: Venue, rfc: RFC) -> dict:
        if venue.name != "thorchain":
            logging.error("venue.name must be `thorchain`")
            return {}

        data = self._get_raw_thorchain_quote(venue=venue, rfc=rfc)
        parsed_data = self._parse_thorchain_data(data=data)

        return parsed_data

    def _parse_changelly_data(self, data: dict) -> dict:
        _in_asset = data["from"].upper() + "." + data["from"].upper()
        _out_asset = data["to"].upper() + "." + data["to"].upper()
        return {
            "ts": time.time(),
            "venue": "changelly",
            "in_asset": _in_asset,
            "in_amount": float(data["amount"]),
            "out_asset": _out_asset,
            "out_amount": float(data["result"]),
        }

    def _get_raw_changelly_quote(self, venue: Venue, rfc: RFC):

        _params = prep_rfc(venue, rfc, self.ASSET_MAP)

        payload = json.dumps(
            {
                "id": "test",
                "jsonrpc": "2.0",
                "method": "getExchangeAmount",
                "params": _params,
            }
        )

        _sign = hmac.new(
            self.changelly_api_secret.encode("utf-8"),
            payload.encode("utf-8"),
            hashlib.sha512,
        ).hexdigest()

        headers = {
            "Content-Type": "application/json",
            "api-key": self.changelly_api_key,
            "sign": _sign,
        }

        data = self._get_quote_base(
            venue=venue, method="POST", headers=headers, data=payload
        )

        # add query params to data to reference in parsing.
        data.update(_params)
        return data

    def changelly_quote(self, venue: Venue, rfc: RFC) -> dict:
        if venue.name != "changelly":
            logging.error("venue.name must be `changelly`")
            return {}

        data = self._get_raw_changelly_quote(venue=venue, rfc=rfc)
        parsed_data = self._parse_changelly_data(data=data)

        return parsed_data

    def _get_raw_changenow_quote(self, venue: Venue, rfc: RFC):
        _params = prep_rfc(venue, rfc, self.ASSET_MAP)

        _rfc_url = f"{venue.base_url}/v1/exchange-amount/{_params['amount']}/{_params['pair']}?api_key={self.changenow_api_key}"

        # since changenow takes all args in the url, we add new field to Venue
        venue.rfc_url = _rfc_url

        data = self._get_quote_base(venue=venue)

        # add query params to data to reference in parsing.
        data.update(_params)

        return data

    def _parse_changenow_data(self, data: dict) -> dict:
        def _parse_pair(pair: str) -> tuple[str]:
            _from = pair.split("_")[0].upper()
            _to = pair.split("_")[-1].upper()

            in_asset = _from + "." + _from
            out_asset = _to + "." + _to

            return in_asset, out_asset

        _in_asset, _out_asset = _parse_pair(data["pair"])

        return {
            "ts": time.time(),
            "venue": "changenow",
            "in_asset": _in_asset,
            "in_amount": float(data["amount"]),
            "out_asset": _out_asset,
            "out_amount": float(data["estimatedAmount"]),
        }

    def changenow_quote(self, venue: Venue, rfc: RFC) -> dict:
        """
        API Documentation:
        https://documenter.getpostman.com/view/8180765/SVfTPnM8?version=latest
        """
        if venue.name != "changenow":
            logging.error("venue.name must be `changenow`")
            return {}

        data = self._get_raw_changenow_quote(venue=venue, rfc=rfc)
        parsed_data = self._parse_changenow_data(data=data)

        return parsed_data

    def _get_raw_letsexchange_quote(self, venue: Venue, rfc: RFC):
        _params = prep_rfc(venue, rfc, self.ASSET_MAP)
        headers = {
            "Content-Type": "application/x-www-form-urlencoded",
        }

        data = self._get_quote_base(
            venue=venue, method="POST", headers=headers, data=_params
        )

        # add query params to data to reference in parsing.
        # since there is an `amount` in the response, we want to change from_amount
        _params["from_amount"] = _params.pop("amount")
        data.update(_params)

        return data

    def _parse_letsexchange_quote(self, data: dict) -> dict:
        """
        {'min_amount': '0.00422420', 'max_amount': 15, 'amount': '0.91407709', 'fee': '0', 'rate': '14.062724461538', 'profit': '0', 'extra_fee_amount': '0', 'rate_id': '', 'rate_id_expired_at': 0, 'applied_promo_code_id': None, 'networks_from': [{'network': 'BTC', 'has_tag': 0}], 'networks_to': [{'network': 'ETH', 'has_tag': 0}], 'deposit_amount_usdt': 1538.7528, 'withdrawal_amount_usdt': 1513.7206061807997}
        """
        _in_asset = data["network_from"].upper() + "." + data["from"].upper()
        _out_asset = data["network_to"].upper() + "." + data["to"].upper()

        return {
            "ts": time.time(),
            "venue": "letsexchange",
            "in_asset": _in_asset,
            "in_amount": float(data["from_amount"]),
            "out_asset": _out_asset,
            "out_amount": float(data["amount"]),
        }

    def letsexchange_quote(self, venue: Venue, rfc: RFC):
        if venue.name != "letsexchange":
            logging.error("venue.name must be `letsexchange`")
            return {}

        data = self._get_raw_letsexchange_quote(venue=venue, rfc=rfc)
        parsed_data = self._parse_letsexchange_quote(data=data)

        return parsed_data

    def _get_raw_cic_quote(self, venue: Venue, rfc: RFC):

        _params = prep_rfc(venue, rfc, self.ASSET_MAP)

        _url = f"{venue.base_url}?from={_params['from']}&to={_params['to']}&amount={_params['amount']}"

        venue.rfc_url = _url

        data = self._get_quote_base(venue=venue)
        data = data.get("fullExchangeAmount")

        return data

    def _parse_cic_quote(self, data: dict) -> dict:
        _in_asset = data["from"].upper() + "." + data["from"].upper()
        _out_asset = data["to"].upper() + "." + data["to"].upper()

        return {
            "ts": time.time(),
            "venue": "cic",
            "in_asset": _in_asset,
            "in_amount": float(data["amount"]),
            "out_asset": _out_asset,
            "out_amount": float(data["result"]),
        }

    def cic_quote(self, venue: Venue, rfc: RFC):
        if venue.name != "cic":
            logging.error("venue.name must be `cic`")
            return {}

        data = self._get_raw_cic_quote(venue=venue, rfc=rfc)
        parsed_data = self._parse_cic_quote(data=data)

        return parsed_data

    def run(self):
        quote_list = []
        for venue in self.venue_list:
            for rfc in self.rfc_list:
                parsed_data = self.getters.get(venue.name)(venue, rfc)

                if parsed_data:
                    quote_list.append(parsed_data)
                time.sleep(1)  # avoid rate limiting

        _df = pd.DataFrame(quote_list)

        _df["quote_time"] = pd.to_datetime(datetime.datetime.now())
        _cols = [
            "quote_time",
            "venue",
            "in_asset",
            "in_amount",
            "out_asset",
            "out_amount",
        ]

        logging.info(
            f"""
        {_df[_cols]}
        """
        )
        _df[_cols].to_csv(f'data/{datetime.datetime.now()}-quotes.csv')

        return _df[_cols]


if __name__ == "__main__":
    from dotenv import load_dotenv

    load_dotenv()

    _venue_list = [
        Venue(
            name="thorchain",
            base_url="https://thornode.ninerealms.com/thorchain/quote/swap",
        ),
        Venue(name="changelly", base_url="https://api.changelly.com"),
        Venue(name="changenow", base_url="https://api.changenow.io"),
        Venue(
            name="letsexchange",
            base_url="https://api.letsexchange.io/api/v1/info?float=true",
        ),
        Venue(
            name="cic",
            base_url="https://api.criptointercambio.com/amount/exchange/full",
        ),
    ]

    _rfc_list = [
        # BTC -> ETH
        RFC(from_asset="BTC.BTC", to_asset="ETH.ETH", from_amount=0.0063),
        RFC(from_asset="BTC.BTC", to_asset="ETH.ETH", from_amount=0.016),
        RFC(from_asset="BTC.BTC", to_asset="ETH.ETH", from_amount=0.034),
        RFC(from_asset="BTC.BTC", to_asset="ETH.ETH", from_amount=0.065),
        RFC(from_asset="BTC.BTC", to_asset="ETH.ETH", from_amount=1.0),
        # ETH -> BTC
        RFC(from_asset="ETH.ETH", to_asset="BTC.BTC", from_amount=0.1),
        RFC(from_asset="ETH.ETH", to_asset="BTC.BTC", from_amount=0.25),
        RFC(from_asset="ETH.ETH", to_asset="BTC.BTC", from_amount=0.5),
        RFC(from_asset="ETH.ETH", to_asset="BTC.BTC", from_amount=1.0),
    ]

    eq = ExecutionQuality(
        venue_list=_venue_list,
        rfc_list=_rfc_list,
    )

    eq.run()
