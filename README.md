# execution_quality

Script to gather quotes from THORChain and Change Services

<br>

## Supported Venues
- THORChain
- Changelly
- ChangeNow
- LetsExchange
- CIC (criptointercambio)
<br>

## Example Output

| quote_time    | venue     | in_asset | in_amount | out_asset | out_amount |
|---------------|-----------|----------|-----------|-----------|------------|
|   <timestamp> | thorchain | BTC.BTC  | 1.0       | ETH.ETH   | 14.6067107 |
|   <timestamp> | changelly | BTC.BTC  | 1.0       | ETH.ETH   | 14.59      |
| ...           | ...       | ...      | ...       | ...       | ...        |

## .env
copy `.example-env` to `.env` file and add your api keys/secrets.
```bash
cp .example-env .env
```
## To run
1. clone repo
2. cd into dir
3. build docker image with `make build`
4. run script with  `make run`
5. csv of data will be output in `/data`
